@extends('layouts.home')

@section('content')
<section class="content-header mt-5">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6 text-light">
                <h1>Edit Profile</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/post" class="text-light">Profile</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="card card-widget" style="background: #ffcbba; margin-top: 10px;">
        <div class="card-header">
            <h3 class="card-title">Edit Profile</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/profile/{{ $profile->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="text-center">
                    @if ($profile->img_url === null)
                    <img class="profile-user-img img-fluid img-circle" src="{{ asset('images/profile-image.jpg') }}"
                        alt="User profile picture">
                    @else
                    <img class="profile-user-img img-fluid img-circle"
                        src="{{ asset('uploads/profile/' . $profile -> img_url) }}" alt="User profile picture">
                    @endif
                </div>
                <div class="form-group">
                    <label for="tags">Full Name</label>
                    <input type="text" class="form-control" id="full_name" name="full_name"
                        value="{{old('full_name', $profile->full_name)}}">
                </div>
                <div class="form-group">
                    <label for="capton">Bio</label>
                    <textarea rows="10" class="form-control" id="bio"
                        name="bio">{{ old('bio', $profile->bio) }}</textarea>
                </div>
                <div class="form-group">
                    <label for="img_url">Profile Picture</label>
                    <input type="file" class="btn btn-light" id="img_url" name="img_url"></input>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer float-right">
                <button type="submit" class="btn btn-light">Edit</button>
            </div>
        </form>
    </div>
</section>
@endsection