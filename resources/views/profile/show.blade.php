@extends('layouts.home')

@section('content')
<section class="content-header mt-5">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6 text-light">
                <h1>Edit Profile</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/post" class="text-light">Post</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="card card-primary card-outline">
        <div class="card-body box-profile">
            <div class="text-center">
                @if ($profile->img_url === null)
                <img class="profile-user-img img-fluid img-circle" src="{{ asset('images/profile-image.jpg') }}"
                    alt="User profile picture">
                @else
                <img class="profile-user-img img-fluid img-circle"
                    src="{{ asset('uploads/profile/' . $profile -> img_url) }}" alt="User profile picture">
                @endif
            </div>

            <h3 class="profile-username text-center">{{ $profile->full_name }}</h3>

            <p class="text-muted text-center">@ {{ $user->username }}</p>

            <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">{{ $user->follower_count() }}</a>
                </li>
                <li class="list-group-item">
                    <b>Following</b> <a class="float-right">{{ $user->following_count() }}</a>
                </li>
                <li class="list-group-item">
                    <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>

                    <p class="text-muted">{{ $profile->bio }}</p>
                </li>
            </ul>

            @if ($profile->user_id !== $user->id && $user->isFollowed())
            <form class="d-inline" action="{{ route('follow.destroy', ['follow' => $user->id]) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-primary btn-block">
                    Unfollow</button>
            </form>
            @elseif ($profile->user_id !== $user->id)
            <form class="d-inline" action="{{ route('follow.store') }}" method="POST">
                @csrf
                <input type="hidden" value="{{ $user->id }}" name="user_id" class="dropdown-item btn btn-light btn-sm">
                <button type="submit" class="btn btn-primary btn-block">Follow</button>
            </form>
            @else
            <a href="{{ route('profile.edit', ['profile' => $profile->id]) }}" class="btn btn-primary btn-block"><b>Edit
                    Profile</b></a>
            @endif

        </div>
        <!-- /.card-body -->
    </div>
</section>
@endsection