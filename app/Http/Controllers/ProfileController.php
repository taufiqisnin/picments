<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Profile;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['index', 'create', 'store', 'edit', 'update', 'destroy']);
    }

    public function create()
    {
        return view('post.create');
    }

    public function show($id)
    {
        $profile = Profile::where('user_id', $id)->first();
        $user = Auth::user();

        return view('profile.show', compact('profile', 'user'));
    }

    public function edit($id)
    {
        $profile = Profile::find($id);
        if ($profile->user_id != Auth::id()) {
            return redirect('profile');
        }


        return view('profile.edit', compact('profile'));
    }

    public function update(Request $request, $id)
    {
        $profile = Profile::find($id);
        if ($profile->user_id != Auth::id()) {
            return redirect('profile');
        }

        if ($request->hasFile('img_url')) {
            $file = $request->file('img_url');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/profile', $filename);

            $update = Profile::where('id', $id)
                ->update([
                    "full_name" => $request["full_name"],
                    "bio" => $request["bio"],
                    "img_url" => $filename
                ]);
        } else {
            /* return $request; */
            $filename = $profile->img_url;

            $update = Profile::where('id', $id)
                ->update([
                    "full_name" => $request["full_name"],
                    "bio" => $request["bio"],
                    "img_url" => $filename
                ]);
        }


        Alert::success('Success', 'Profile Edited');
        return redirect('post');
    }
}
